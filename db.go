type DB struct {
	master       *sql.DB
	slaves []interface{}
	count        int
}

func NewDB(master *sql.DB, slaves ...interface{}) *DB {
	return &DB{
		master:       master,
		slaves: slaves,
	}
}

func (db *DB) slaveHelper() *sql.DB {
	db.count++
	return db.slaves[db.count%len(db.slaves)].(*sql.DB)
}

func (db *DB) Ping() error {
	if err := db.master.Ping(); err != nil {
		panic(err)
	}

	for i := range db.slaves {
		if err := db.slaves[i].(*sql.DB).Ping(); err != nil {
			panic(err)
		}
	}

	return nil
}

func (db *DB) PingContext(ctx context.Context) error {
	if err := db.master.PingContext(ctx); err != nil {
		panic(err)
	}

	for i := range db.slaves {
		if err := db.slaves[i].(*sql.DB).PingContext(ctx); err != nil {
			panic(err)
		}
	}

	return nil
}

func (db *DB) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return db.slaveHelper().Query(query, args...)
}

func (db *DB) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	return db.slaveHelper().QueryContext(ctx, query, args...)
}

func (db *DB) QueryRow(query string, args ...interface{}) *sql.Row {
	return db.slaveHelper().QueryRow(query, args...)
}

func (db *DB) QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row {
	return db.slaveHelper().QueryRowContext(ctx, query, args...)
}

func (db *DB) Begin() (*sql.Tx, error) {
	return db.master.Begin()
}

func (db *DB) BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error) {
	return db.master.BeginTx(ctx, opts)
}

func (db *DB) Close() error {
	db.master.Close()
	for i := range db.slaves {
		db.slaves[i].(*sql.DB).Close()
	}
	return nil
}

func (db *DB) Exec(query string, args ...interface{}) (sql.Result, error) {
	return db.master.Exec(query, args...)
}

func (db *DB) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return db.master.ExecContext(ctx, query, args...)
}

func (db *DB) Prepare(query string) (*sql.Stmt, error) {
	return db.master.Prepare(query)
}

func (db *DB) PrepareContext(ctx context.Context, query string) (*sql.Stmt, error) {
	return db.master.PrepareContext(ctx, query)
}

func (db *DB) SetConnMaxLifetime(duration time.Duration) {
	db.master.SetConnMaxLifetime(duration)
	for i := range db.slaves {
		db.slaves[i].(*sql.DB).SetConnMaxLifetime(duration)
	}
}

func (db *DB) SetMaxIdleConns(idleConnections int) {
	db.master.SetMaxIdleConns(idleConnections)
	for i := range db.slaves {
		db.slaves[i].(*sql.DB).SetMaxIdleConns(idleConnections)
	}
}

func (db *DB) SetMaxOpenConns(connections int) {
	db.master.SetMaxOpenConns(connections)
	for i := range db.slaves {
		db.readreplicas[i].(*sql.DB).SetMaxOpenConns(connections)
	}
}